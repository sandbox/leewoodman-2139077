<?php
/**
 * @file
 * Contains the administrative functions of the captify module.
 *
 * This file is included by the core captify module, and includes the
 * settings form.
 */

/**
 * Implements hook_admin_settings_form().
 */
function captify_admin_settings_form() {
  $form = array();

  // Define where Captify JavaScript renders on the page.
  $form['captify_js_scope'] = array(
    '#type' => 'select',
    '#title' => t('JavaScript Scope'),
    '#options' => _captify_get_allowed_js_scopes_options(),
    '#default_value' => _captify_get_js_scope_setting(),
    '#description' => t('Do you want to place the Javascript in the footer or the header of the page?'),
  );

  // Captify ID.
  $form['captify_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Captify JavaScript ID'),
    '#size' => 10,
    '#maxlength' => 10,
    '#required' => TRUE,
    '#default_value' => variable_get('captify_id'),
    '#description' => t('Enter the ID contained in the Javascript. Example: data.captifymedia.com/654321.js. Enter 654321 in the field below.'),
  );

  return system_settings_form($form);
}
