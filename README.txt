
Module: Captify

Description
===========
Provides Captify JavaScript snippet in the footer of all pages


Installation
============
Copy the 'captify' module directory in to your Drupal.
/sites/all/modules directory as usual.

The inline JavaScript will be placed in the footer of the page with a weight 
of 0 by default.


Configuration
====================================================
Captify can be configured using the admin settings form:
/admin/config/system/captify

The "captify id" should be set, which will be specific to your application and 
available from your Captify account manager.

The scope of the JavaScript can be Header or Footer.
